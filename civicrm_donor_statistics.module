<?php

/**
 * Implements hook_permission().
 */
function civicrm_donor_statistics_permission() {
  return array(
    'export donor stats' => array(
      'title' => t('Export donor stats'),
      'description' => t('Export donor stats as a CSV file.'),
    ),
  );
}

function civicrm_donor_statistics_menu() {
  // Admin
  $items['civicrm/donor_stats_export/overview'] = array(
    'title' => 'Export donor stats overview',
    'description' => 'Export donor stats overview as a CSV file.',
    'page callback' => 'civicrm_donor_statistics_overview',
    'access arguments' => array('export donor stats'),
  );
  $items['civicrm/donor_stats_export/new'] = array(
    'title' => 'Export new donor details',
    'description' => 'Export new donor details as a CSV file.',
    'page callback' => 'civicrm_donor_statistics_new',
    'access arguments' => array('export donor stats'),
  );
  $items['civicrm/donor_stats_export'] = array(
    'title' => 'Export lapsed donor details',
    'description' => 'Export lapsed donor details as a CSV file.',
    'page callback' => 'civicrm_donor_statistics',
    'access arguments' => array('export donor stats'),
  ); 
  return $items;
}

function civicrm_donor_statistics_overview() {
  $headers = array('Content-type' => 'text/octect-stream', 'Content-Disposition' => 'attachment;filename=data.csv');
  
  header('Content-Type: text/x-csv'); 
  header('Expires: '. gmdate('D, d M Y H:i:s') .' GMT');
  header('Content-Disposition: inline; filename="overview.csv"');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');

  $source = '"","NEW","LAPSED","UPGRADED","DOWNGRADED","MAINTAINED"';
  $source .= "\n";  
  
  $filters = arg(); //grab all arguments
  $filters = array_slice($filters, 3); //narrow to arguments that may be filters
  
  //new donors prep query: this one is easy lemon squeezy - get donors whose first contribution date is in the specified year
  $query_string = "SELECT cc.contact_id, MIN(cc.receive_date) AS receive_date FROM {civicrm_contribution} cc ";  
  if($filters){
    $query_string .= "WHERE cc.source IN('".implode("','", $filters)."') ";
  }
  $query_string .= "GROUP BY contact_id HAVING receive_date >= :start and receive_date < :end";  
  
  //lapsed/upgraded/downgraded/maintained donors prep query
  $query_string_2 = "SELECT cc.contact_id, SUM(total_amount) as total FROM {civicrm_contribution} cc 
                     WHERE receive_date >= :start AND receive_date < :end ";  
  if($filters){
    $query_string_2 .= "AND cc.source IN('".implode("','", $filters)."') ";
  }
  $query_string_2 .= "GROUP BY contact_id";  
  
  db_set_active('civicrm'); //tell drupal to use civicrm database
  
  for($year=2009; $year<=2012; $year++){
	  $source .= '"'.$year.'",';
	  
	  //set up and format dates
	  $start = new DateTime($year.'-01-01');
	  $end = new DateTime($year.'-01-01');
	  $end->add(new DateInterval('P1Y'));
	  $start = $start->format('Y-m-d H:i:s');
	  $end = $end->format('Y-m-d H:i:s');
	  $start_prev = new DateTime($year.'-01-01');
	  $end_prev = new DateTime($year.'-01-01');
	  $start_prev->sub(new DateInterval('P1Y'));
	  $start_prev = $start_prev->format('Y-m-d H:i:s');
	  $end_prev = $end_prev->format('Y-m-d H:i:s');
                        
    //new donors: run previously-prepped query string with appropriate dates                    
    $result = db_query($query_string, array(':start' => $start, ':end' => $end));                    
                        
	  $count = 0;
	  foreach($result as $row){
	    $count++;
	  }
	  $source .= '"'.$count.'",';
	  
	  //lapsed/upgraded/downgraded/maintained donors: run previously-prepped query strings with appropriate dates
	  $result = db_query($query_string_2, array(':start' => $start, ':end' => $end));
	  $result_prev = db_query($query_string_2, array(':start' => $start_prev, ':end' => $end_prev));
		
		$lapsed = 0;
		$upgraded = 0;
		$downgraded = 0;
		$maintained = 0;
		$result_array = array();
		$result_prev_array = array();
		//set up two arrays for comparing
	  foreach($result as $row){
	    $result_array[$row->contact_id] = $row->total;
	  }
	  foreach($result_prev as $row){
	    $result_prev_array[$row->contact_id] = $row->total;
	  }
	  
	  foreach($result_prev_array as $contact_id => $total){
		  if(!array_key_exists($contact_id,$result_array)){
		    $lapsed++;
		  }
		}
		
	  foreach($result_array as $contact_id => $total){
		  if(array_key_exists($contact_id,$result_prev_array) && $total > $result_prev_array[$contact_id]){
		    $upgraded++;
		  }
		  elseif(array_key_exists($contact_id,$result_prev_array) && $total < $result_prev_array[$contact_id]){
		    $downgraded++;
		  }
		  elseif(array_key_exists($contact_id,$result_prev_array) && $total == $result_prev_array[$contact_id]){
		    $maintained++;
		    //echo $contact_id. ', ';
		  }
		}
	  $source .= '"'.$lapsed.'","'.$upgraded.'","'.$downgraded.'","'.$maintained.'",';
	  $source .= "\n";
  }//end for $year
  
  db_set_active(); //release current database
  echo $source;
}

function civicrm_donor_statistics_new() {
  $headers = array('Content-type' => 'text/octect-stream', 'Content-Disposition' => 'attachment;filename=new.csv');
  
  header('Content-Type: text/x-csv'); 
  header('Expires: '. gmdate('D, d M Y H:i:s') .' GMT');
  header('Content-Disposition: inline; filename="new.csv"');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');

  $source = '"YEAR","CONTACT ID","NAME","EMAIL","SUM"';
  $source .= "\n";  
  
  $filters = arg(); //grab all arguments
  $filters = array_slice($filters, 3); //narrow to arguments that may be filters
  
  //prep query: this one is easy lemon squeezy - get donors whose first contribution date is in the specified year
  //DEAR GODS: this segment needs a separate query to calculate total donations for the new donor for that year only, like the others (lapsed, et al) do 
  $query_string = "SELECT cc.contact_id, MIN(cc.receive_date) AS receive_date, SUM(cc.total_amount) AS sum, civicrm_contact.display_name, civicrm_email.email 
                      FROM {civicrm_contribution} cc 
                      LEFT JOIN civicrm_contact ON cc.contact_id = civicrm_contact.id
                      LEFT JOIN civicrm_email ON cc.contact_id = civicrm_email.contact_id ";
  if($filters){
    $query_string .= "WHERE cc.source IN('".implode("','", $filters)."') ";
  }
  $query_string .= "GROUP BY contact_id HAVING receive_date >= :start and receive_date < :end";
  
  db_set_active('civicrm'); //tell drupal to use civicrm database
  
  for($year=2009; $year<=2012; $year++){
	  
	  //set up and format dates
	  $start = new DateTime($year.'-01-01');
	  $end = new DateTime($year.'-01-01');
	  $end->add(new DateInterval('P1Y'));
	  $start = $start->format('Y-m-d H:i:s');
	  $end = $end->format('Y-m-d H:i:s');

    //run previously-prepped query string with appropriate dates
	  $result = db_query($query_string, array(':start' => $start, ':end' => $end));
																				
    foreach($result as $row){
			$source .= '"'.$year.'",';
			
	    $source .= '"'.$row->contact_id.'","'.$row->display_name.'","'.$row->email.'","'.$row->sum.'",';
	    $source .= "\n";
	  }  

		$source .= "\n";		
  }//end for $year
  
	db_set_active(); //release current database
  echo $source;
}

function civicrm_donor_statistics($group) {  //function handles lapsed/upgraded/downgraded/maintained
  $headers = array('Content-type' => 'text/octect-stream', 'Content-Disposition' => 'attachment;filename='.$group.'.csv');
  
  header('Content-Type: text/x-csv'); 
  header('Expires: '. gmdate('D, d M Y H:i:s') .' GMT');
  header('Content-Disposition: inline; filename="'.$group.'.csv"');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');

  $source = '"YEAR","CONTACT ID","NAME","EMAIL","SUM"';
  $source .= "\n";  
  
  $filters = arg(); //grab all arguments
  $filters = array_slice($filters, 3); //narrow to arguments that may be filters
  
  //lapsed/upgraded/downgraded/maintained donors: run previously-prepped query strings with appropriate dates
  $query_string = "SELECT cc.contact_id, SUM(total_amount) as total FROM {civicrm_contribution} cc 
                   WHERE receive_date >= :start and receive_date < :end ";
  if($filters){
    $query_string .= "AND cc.source IN('".implode("','", $filters)."') ";
  }
  $query_string .= "GROUP BY contact_id";  
  
  db_set_active('civicrm'); //tell drupal to use civicrm database
  
  for($year=2009; $year<=2012; $year++){
	  
	  //set up and format dates
	  $start = new DateTime($year.'-01-01');
	  $end = new DateTime($year.'-01-01');
	  $end->add(new DateInterval('P1Y'));
	  $start = $start->format('Y-m-d H:i:s');
	  $end = $end->format('Y-m-d H:i:s');
	  $start_prev = new DateTime($year.'-01-01');
	  $end_prev = new DateTime($year.'-01-01');
	  $start_prev->sub(new DateInterval('P1Y'));
	  $start_prev = $start_prev->format('Y-m-d H:i:s');
	  $end_prev = $end_prev->format('Y-m-d H:i:s');

	  //get donors and total amount donated current year and year before
	  $result = db_query($query_string, array(':start' => $start, ':end' => $end));
	  $result_prev = db_query($query_string, array(':start' => $start_prev, ':end' => $end_prev));												
		
		$result_array = array();
		$result_prev_array = array();
		$contact_donations = array();
		$contact_ids = array();
		//set up two arrays for comparing
	  foreach($result as $row){
	    $result_array[$row->contact_id] = $row->total;
	  }
	  foreach($result_prev as $row){
	    $result_prev_array[$row->contact_id] = $row->total;
	  }
	  
    if($group == 'lapsed'){
      foreach($result_prev_array as $contact_id => $total){
        if(!array_key_exists($contact_id,$result_array)){
          $contact_donations[$contact_id] = $total; //save total for display later
          $contact_ids[] = $contact_id;  //save contact ids for querying name and email
        }
      }
    }
    elseif($group == 'upgraded'){
      foreach($result_array as $contact_id => $total){
        if(array_key_exists($contact_id,$result_prev_array) && $total > $result_prev_array[$contact_id]){
          $contact_donations[$contact_id] = $total; //save total for display later
          $contact_ids[] = $contact_id;  //save contact ids for querying name and email
        }
      }
    }  
    elseif($group == 'downgraded'){
      foreach($result_array as $contact_id => $total){
        if(array_key_exists($contact_id,$result_prev_array) && $total < $result_prev_array[$contact_id]){
          $contact_donations[$contact_id] = $total; //save total for display later
          $contact_ids[] = $contact_id;  //save contact ids for querying name and email
        }
      }
    }
    elseif($group == 'maintained'){
      foreach($result_array as $contact_id => $total){
        if(array_key_exists($contact_id,$result_prev_array) && $total == $result_prev_array[$contact_id]){
          $contact_donations[$contact_id] = $total; //save total for display later
          $contact_ids[] = $contact_id;  //save contact ids for querying name and email
        }
      }
    }
      		
		if($contact_ids){ //we have contact; grab name and email
		  $result = db_query("SELECT civicrm_contact.id, civicrm_contact.display_name, civicrm_email.email 
												FROM civicrm_contact
												LEFT JOIN civicrm_email ON civicrm_contact.id = civicrm_email.contact_id
												WHERE (civicrm_email.is_primary = '1' OR civicrm_email.is_primary IS NULL) 
												AND civicrm_contact.id IN (:contacts)",
												array(':contacts' => $contact_ids)); 
																																
      foreach($result as $row){
			  $source .= '"'.$year.'",';
		    $source .= '"'.$row->id.'","'.$row->display_name.'","'.$row->email.'","'.$contact_donations[$row->id].'",';
		    $source .= "\n";
		  }  
		}
		else 
		  $source .= '"'.$year.'"' . "\n";
		
		$source .= "\n";		
  }//end for $year

	db_set_active(); //release current database  
  echo $source;
}


